package pl.piotrwierzbowski.digiteum.controller.model;

import java.time.LocalDate;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.PastOrPresent;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class EmployeeCreateRequest{

	@NotNull
	private String firstName;
	
	@NotNull
	private String lastName;
	
	private String contractInformation;
	
	@PastOrPresent
	private LocalDate birthDate;
}
