package pl.piotrwierzbowski.digiteum.controller.model;

import javax.validation.constraints.NotNull;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class EmployeeStateUpdateRequest {

	@NotNull
	private StateDTO state;
}
