package pl.piotrwierzbowski.digiteum.controller.model;

public enum StateDTO {

	ADDED,
	IN_CHECK,
	APPROVED,
	ACTIVE;
}
