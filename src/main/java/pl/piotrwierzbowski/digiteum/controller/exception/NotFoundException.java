package pl.piotrwierzbowski.digiteum.controller.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(code = HttpStatus.NOT_FOUND, reason = "resource not found")
public class NotFoundException extends Exception {

	private static final long serialVersionUID = -37005560446618280L;

	public NotFoundException() {
	}

	public NotFoundException(Throwable cause) {
		super(cause);
	}

}
