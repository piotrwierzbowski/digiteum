package pl.piotrwierzbowski.digiteum.controller;


import java.net.URI;

import javax.persistence.EntityNotFoundException;
import javax.validation.Valid;

import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import pl.piotrwierzbowski.digiteum.controller.exception.NotFoundException;
import pl.piotrwierzbowski.digiteum.controller.model.EmployeeCreateRequest;
import pl.piotrwierzbowski.digiteum.controller.model.EmployeeStateUpdateRequest;
import pl.piotrwierzbowski.digiteum.controller.model.StateDTO;
import pl.piotrwierzbowski.digiteum.domain.Employee;
import pl.piotrwierzbowski.digiteum.domain.State;
import pl.piotrwierzbowski.digiteum.service.EmployeeService;

@Log4j2
@RestController
@RequestMapping(consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
@RequiredArgsConstructor
public class EmployeeController {

	private final EmployeeService employeeService;
	
	@PostMapping(value = "/employee")
	public ResponseEntity<Void> addEmployee(@Valid @RequestBody EmployeeCreateRequest employeeCreateRequest) {
		Employee newEmployee = employeeCreateRequestToEmployee(employeeCreateRequest);
		
		int id = employeeService.saveEmployee(newEmployee);
		
		URI location = ServletUriComponentsBuilder
                .fromCurrentRequest()
                .path("/{id}")
                .buildAndExpand(id)
                .toUri();
		
		return ResponseEntity.created(location)
				.build();
	}
	
	@PutMapping(value = "/employee/{id}/state")
	public void changeEmployeeState(@PathVariable int id, @Valid @RequestBody EmployeeStateUpdateRequest employeeStateUpdateRequest) throws NotFoundException {
		try {
			employeeService.updateState(id, stateDTOToState(employeeStateUpdateRequest.getState()));
		} catch (EntityNotFoundException e) {
			log.error(e.getMessage(), e);
			throw new NotFoundException(e);
		}
	}
	
	private Employee employeeCreateRequestToEmployee(EmployeeCreateRequest employeeCreateRequest) {
		return Employee.builder()
				.birthDate(employeeCreateRequest.getBirthDate())
				.contractInformation(employeeCreateRequest.getContractInformation())
				.firstName(employeeCreateRequest.getFirstName())
				.lastName(employeeCreateRequest.getLastName())
				.build();
	}
	
	private State stateDTOToState(StateDTO state) {
		return Enum.valueOf(State.class, state.name());
	}
}
