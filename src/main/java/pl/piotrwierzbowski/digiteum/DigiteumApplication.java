package pl.piotrwierzbowski.digiteum;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DigiteumApplication {

	public static void main(String[] args) {
		SpringApplication.run(DigiteumApplication.class, args);
	}

}
