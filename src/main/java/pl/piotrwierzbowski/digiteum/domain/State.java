package pl.piotrwierzbowski.digiteum.domain;

public enum State {

	ADDED,
	IN_CHECK,
	APPROVED,
	ACTIVE;
}
