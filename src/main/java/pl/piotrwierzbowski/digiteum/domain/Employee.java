package pl.piotrwierzbowski.digiteum.domain;

import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneOffset;
import java.util.Objects;
import java.util.Optional;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Builder
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Employee {

	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	private Integer id;
	private String firstName;
	private String lastName;
	private String contractInformation;
	private LocalDate birthDate;
	private State state;
	
	public Optional<Integer> getAge() {
		if (Objects.nonNull(birthDate)) {
			Period period = Period.between(birthDate, LocalDate.ofInstant(Instant.now(), ZoneOffset.UTC));
			return Optional.of(period.getYears());
		} else {
			return Optional.empty();
		}
	}
}
