package pl.piotrwierzbowski.digiteum.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import pl.piotrwierzbowski.digiteum.domain.Employee;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer>{

}
