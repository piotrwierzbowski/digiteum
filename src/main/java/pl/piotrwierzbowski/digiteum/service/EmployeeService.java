package pl.piotrwierzbowski.digiteum.service;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import pl.piotrwierzbowski.digiteum.dao.EmployeeRepository;
import pl.piotrwierzbowski.digiteum.domain.Employee;
import pl.piotrwierzbowski.digiteum.domain.State;

@Log4j2
@Transactional
@Service
@RequiredArgsConstructor
public class EmployeeService {

	private final EmployeeRepository employeeRepository;
	
	public int saveEmployee(Employee employee) {
		employee.setState(State.ADDED);
		Employee newEmployee = employeeRepository.save(employee);
		
		log.info("Saved new employee with id {}", newEmployee.getId());
		
		return newEmployee.getId();
	}
	
	public void updateState(int id, State newState) {
		Employee employee = employeeRepository.getById(id);
		State oldState = employee.getState();
		
		employee.setState(newState);
		employeeRepository.save(employee);
		
		log.info("Changed state for employee id {} from {} to {}", id, oldState, newState);
	}
}
