package pl.piotrwierzbowski.digiteum.controller;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.time.LocalDate;

import javax.persistence.EntityNotFoundException;

import org.hamcrest.core.StringEndsWith;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;

import pl.piotrwierzbowski.digiteum.controller.model.EmployeeCreateRequest;
import pl.piotrwierzbowski.digiteum.controller.model.EmployeeStateUpdateRequest;
import pl.piotrwierzbowski.digiteum.controller.model.StateDTO;
import pl.piotrwierzbowski.digiteum.domain.State;
import pl.piotrwierzbowski.digiteum.service.EmployeeService;

@ExtendWith(SpringExtension.class)
@WebMvcTest
@AutoConfigureMockMvc
class EmployeeControllerTest {

	@MockBean
	private EmployeeService employeeService;
	
    @Autowired
    private MockMvc mockMvc;
    
    @Autowired
    private ObjectMapper objectMapper;
	
	@Test
	void testAddEmployeeInvalid() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post("/employee")
			      .content("{}")
			      .contentType(MediaType.APPLICATION_JSON))
			      .andExpect(MockMvcResultMatchers.status().isBadRequest())
			      .andExpect(MockMvcResultMatchers.content().contentType(MediaType.APPLICATION_JSON));
	}
	
	@Test
	void testAddEmployee() throws Exception {
		// arrange
		EmployeeCreateRequest request = EmployeeCreateRequest.builder()
				.birthDate(LocalDate.of(1983, 5, 23))
				.firstName("irrelevant")
				.lastName("irrelevant")
				.build();
		String body = objectMapper.writeValueAsString(request);
		
		when(employeeService.saveEmployee(any())).thenReturn(1);
		
		// assert
		mockMvc.perform(MockMvcRequestBuilders.post("/employee")
			      .content(body)
			      .contentType(MediaType.APPLICATION_JSON))
			      .andExpect(MockMvcResultMatchers.status().isCreated())
			      .andExpect(MockMvcResultMatchers.header().string("Location", StringEndsWith.endsWith("1")));
	}

	@Test
	void testChangeEmployeeState() throws Exception {
		// arrange
		EmployeeStateUpdateRequest request = EmployeeStateUpdateRequest.builder()
				.state(StateDTO.IN_CHECK)
				.build();
		String body = objectMapper.writeValueAsString(request);
		
		// assert
		mockMvc.perform(MockMvcRequestBuilders.put("/employee/{id}/state", 1)
			      .content(body)
			      .contentType(MediaType.APPLICATION_JSON))
			      .andExpect(MockMvcResultMatchers.status().isOk())
			      .andExpect(MockMvcResultMatchers.jsonPath("$").doesNotExist());
		
		verify(employeeService).updateState(anyInt(), eq(State.IN_CHECK));
	}
	
	@Test
	void testChangeEmployeeStateNoEntity() throws Exception {
		// arrange
		EmployeeStateUpdateRequest request = EmployeeStateUpdateRequest.builder()
				.state(StateDTO.IN_CHECK)
				.build();
		String body = objectMapper.writeValueAsString(request);
		
		doThrow(new EntityNotFoundException()).when(employeeService).updateState(anyInt(), any());
		
		// assert
		mockMvc.perform(MockMvcRequestBuilders.put("/employee/{id}/state", 1)
			      .content(body)
			      .contentType(MediaType.APPLICATION_JSON))
			      .andExpect(MockMvcResultMatchers.status().isNotFound());
		
	}

}
