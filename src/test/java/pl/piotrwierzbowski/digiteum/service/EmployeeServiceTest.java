package pl.piotrwierzbowski.digiteum.service;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalDate;
import java.util.Optional;

import javax.persistence.EntityNotFoundException;

import org.apache.commons.lang3.RandomStringUtils;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import pl.piotrwierzbowski.digiteum.dao.EmployeeRepository;
import pl.piotrwierzbowski.digiteum.domain.Employee;
import pl.piotrwierzbowski.digiteum.domain.State;

@ExtendWith(SpringExtension.class)
@SpringBootTest
class EmployeeServiceTest {

	@Autowired
	private EmployeeRepository employeeRepository;
	
	@Autowired
	private EmployeeService employeeService;

	@Test
	void testSaveEmployee() {
		// given
		String irrelevantString = RandomStringUtils.randomAlphanumeric(10);
		Employee employee = Employee.builder()
				.birthDate(LocalDate.now())
				.contractInformation(irrelevantString)
				.firstName(irrelevantString)
				.lastName(irrelevantString)
				.build();
		
		Employee expectedEmployee = Employee.builder()
				.birthDate(LocalDate.now())
				.contractInformation(irrelevantString)
				.firstName(irrelevantString)
				.lastName(irrelevantString)
				.state(State.ADDED)
				.build();
		
		// when
		int id = employeeService.saveEmployee(employee);
		expectedEmployee.setId(id);
		Optional<Employee> savedEmployee = employeeRepository.findById(id);

		// then
		assertThat(savedEmployee).isNotEmpty();
		assertThat(savedEmployee.get()).isEqualTo(expectedEmployee);
	}

	@Test
	void testUpdateState() {
		// given
		String irrelevantString = RandomStringUtils.randomAlphanumeric(10);
		Employee employee = Employee.builder()
				.birthDate(LocalDate.now())
				.contractInformation(irrelevantString)
				.firstName(irrelevantString)
				.lastName(irrelevantString)
				.build();
		
		Employee expectedEmployee = Employee.builder()
				.birthDate(LocalDate.now())
				.contractInformation(irrelevantString)
				.firstName(irrelevantString)
				.lastName(irrelevantString)
				.state(State.IN_CHECK)
				.build();
		
		// when
		int id = employeeService.saveEmployee(employee);
		expectedEmployee.setId(id);
		employeeService.updateState(id, State.IN_CHECK);
		Optional<Employee> savedEmployee = employeeRepository.findById(id);

		// then
		assertThat(savedEmployee).isNotEmpty();
		assertThat(savedEmployee.get()).isEqualTo(expectedEmployee);
	}
	
	@Test
	void testUpdateStateNotFound() {
		// given
		String irrelevantString = RandomStringUtils.randomAlphanumeric(10);
		Employee employee = Employee.builder()
				.birthDate(LocalDate.now())
				.contractInformation(irrelevantString)
				.firstName(irrelevantString)
				.lastName(irrelevantString)
				.build();
		
		// when
		int id = employeeService.saveEmployee(employee);
		
		// then
		assertThrows(EntityNotFoundException.class, () -> employeeService.updateState(id + 1, State.IN_CHECK));
	}

}
