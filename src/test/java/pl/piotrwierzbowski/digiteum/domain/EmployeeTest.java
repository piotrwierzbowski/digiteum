package pl.piotrwierzbowski.digiteum.domain;

import static org.assertj.core.api.Assertions.assertThat;

import java.time.LocalDate;
import java.util.Optional;

import org.junit.jupiter.api.Test;

class EmployeeTest {

	@Test
	void testGetAge() {
		// given
		Employee employee = Employee.builder()
				.birthDate(LocalDate.now().minusYears(10))
				.build();
		
		// when
		Optional<Integer> age = employee.getAge();
		
		// then
		assertThat(age).isNotEmpty();
		assertThat(age.get()).isEqualTo(10);
	}
	
	@Test
	void testGetAgeEmpty() {
		// given
		Employee employee = new Employee();
		
		// when
		Optional<Integer> age = employee.getAge();
		
		// then
		assertThat(age).isEmpty();
	}

}
