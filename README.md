# Task description
PeopleFlow (www.pplflw.com) is a global HR platform enabling companies to hire & onboard their employees internationally, at the push of a button. It is our mission to create opportunities for anyone to work from anywhere. As work is becoming even more global and remote, there has never been a bigger chance to build a truly global HR-tech company.


As a part of our backend engineering team, you will be responsible for building our core platform including an  employees managment system.

The employees on this system are assigned to different states, Initially when an employee is added it will be assigned "ADDED" state automatically .


The other states (State machine) for A given Employee are:
- ADDED
- IN-CHECK
- APPROVED
- ACTIVE

Our backend stack is:
- Java 11 
- Spring Framework 
- Kafka


**First Part:**


Your task is to build  Restful API doing the following:
- An Endpoint to support adding an employee with very basic employee details including (name, contract information, age, you can decide.) With initial state "ADDED" which incidates that the employee isn't active yet.

- Another endpoint to change the state of a given employee to "In-CHECK" or any of the states defined above in the state machine 

Please provide a solution with the  above features with the following consideration.

- Being simply executable with the least effort Ideally using Docker and docker-compose or any smailiar approach.
- For state machine could be as simple as of using ENUM or by using https://projects.spring.io/spring-statemachine/ 
- Please provide testing for your solution.
- Providing an API Contract e.g. OPENAPI spec. is a big plus

**Second Part (Optional but a plus):**

Being concerned about developing high quality, resilient software, giving the fact, that you will be participating, mentoring other engineers in the coding review process.


- Suggest what will be your silver bullet, concerns while you're reviewing this part of the software that you need to make sure is being there.
- What the production-readiness criteria that you consider for this solution





**Third Part (Optional but a plus):**
Another Team in the company is building another service, This service will be used to provide some statistics of the employees, this could be used to list the number of employees per country, other types of statistics which is very vague at the moment.


- Please think of a solution without any further implementation that could be able to integrate on top of your service, including the integration pattern will be used, the database storage etc.

A high-level architecture diagram is sufficient to present this.

# Requirements
- Java 11
- Gradle
- Docker
- Docker compose

# Build & Test
Execute the following command
`./gradlew clean build`

Teh application is based on a standard spring boot gradle template, the tests are in the usual place.

# Create docker
Execute the following command
`docker build -t digiteum:latest .`

# Deploy with docker compose
Execute the following command
`docker-compose up -d -V`

After a while a service along with mysql database should be up and running

# Use the resources
The documentation is available via swagger just lunch:
[http://localhost:8080/swagger-ui/](http://localhost:8080/swagger-ui/)
the OPEN API is available at: [http://localhost:8080/v2/api-docs](http://localhost:8080/v2/api-docs) (excuse lack of description but my time was limited :))

# Part 2
### Suggest what will be your silver bullet, concerns while you're reviewing this part of the software that you need to make sure is being there
The solution for the given requirements shall be as simple as possible to make it easily extensible when more requirements will come.
I divided API model from domain model as this very frequently changes and domain model should not operate on DTO objects directly.
Another concern would be to have a quick access to endpoints documentation, that would potentially require exposing Open API endpoints somewhere in the infrastructure.
Error handling is also important and it is good not to mix domain errors with HTTP statuses errors - I separated those in my code.
API responses and errors are based on the standards and best practices in REST.
It is also quite important to log information to logs and on a proper level.
Last but not least test coverage should be high for the part related with business logic.

### What the production-readiness criteria that you consider for this solution
1. The functionality should cover at least some MVP for the product (the business should definitely prepare more than that, perhaps include also compliance requirements, this would probably mean more subsystems)
2. The solution shall be tested and left without major bugs - the best would be to use the test pyramid and automate tests
3. The performance should be sufficient - this is typically conducted in a later phases but it is good to keep such verification in mind
4. Security should be implemented here, it seems that the system will contain a lot of personal data so this aspect should be treated with priority
5. Infrastructure and support should be in place - this would include some CD/CD pipelines as well along with infrastructure monitoring and some support plan, perhaps some support SLA established
6. Backups and recovery for infrastructure and data - the system should be tested against failures and recovery procedures shall be tested

# Part 3
Another Team in the company is building another service, This service will be used to provide some statistics of the employees, this could be used to list the number of employees per country, other types of statistics which is very vague at the moment.

- Please think of a solution without any further implementation that could be able to integrate on top of your service, including the integration pattern will be used, the database storage etc.

A high-level architecture diagram is sufficient to present this.
### Description
The general idea would be to add business events propagation from Employees service to the new statistics module, each business event in Employees (for example create, update, delete) would be send over massage broker (Kafka as suggested in your techstack) and persisted in a dedicated statistical database - ElasticSearch could be used as it offers quite advanced analytical functionalities and employee database remains as a primary source of data (alternatively some other NoSQL DB could be used depending on more accurate requirements).
The main reason why data storages for for employee and statistics shall be decoupled is a different purpose of each system and a possibility that statistical queries would influence employees data storage performance  (those most like query over big sets of data)
### Component Diagram
![Diagram](design/Digiteum.png)