FROM openjdk:11-jdk

COPY build/libs/*.jar ./app.jar

#EXPOSE 8000
EXPOSE 8080

#ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-agentlib:jdwp=transport=dt_socket,server=y,suspend=n,address=*:8000","-jar","/app.jar"]
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/app.jar"]